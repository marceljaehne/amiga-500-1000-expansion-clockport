# Expansion Clockport for Amiga 500 and 1000

This little project adds an A1200 Clockport to your Amiga 500/1000.\
Its fully compatible to clockport hardware and works on standard address $D80000\
\
![Alt-Text](https://gitlab.com/marceljaehne/amiga-500-1000-expansion-clockport/-/raw/aa1535c8c13dcece21c6fcd046d8781b3614cea9/Pictures/IMG_3503.jpg)
\
\
**DISCLAIMER!!!!**\
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA OR HARDWARE!**


