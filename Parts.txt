Partlist

Exported from Clockport.sch at 11.07.2023 22:56

EAGLE Version 9.6.2 Copyright (c) 1988-2020 Autodesk, Inc.

Assembly variant: 

Part     Value                      Device                     Package              Library     Sheet

C1       100n                       C-EUC1206                  C1206                resistor    1
C2       100n                       C-EUC1206                  C1206                resistor    1
C3       100n                       C-EUC1206                  C1206                resistor    1
CN1      CLOCKPORT                  A1200-CLOCKPORT            CP-2X11-2.00         Amiga Matze 1
CN2      A500-EXPANSION-SLOT-SOLDER A500-EXPANSION-SLOT-SOLDER LKONT_43-SLOT-SOLDER Amiga Matze 1
R1       4k7                        R-EU_R1206                 R1206                rcl         1
U1       74HCT688DW                 74HCT688DW                 SO20W                74xx-eu     1
U2       74ACT00D                   74ACT00D                   SO14                 74xx-eu     1
U3       74ACT245DW                 74ACT245DW                 SO20W                74xx-eu     1
U4       74ACT245DW                 74ACT245DW                 SO20W                74xx-eu     1

1 x Card Edge Connector 86pin female 2.54mm
